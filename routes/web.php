<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

Route::get('/', function () {
    return view('welcome');
});


Route::group([
  'prefix'     => 'admin',
  'middleware' => ['web']
], function () {
  Route::post('report-import','AdminReportsController@import', function () {
  });
  Route::get('reports-seeding','AdminReportsController@reportSeeding', function () {
  });
});


Route::group([
  'prefix'     => 'admin',
  'middleware' => ['web']
], function () {
  Route::post('cross-import','AdminCrosspointsController@import', function () {
  });
  Route::get('cross-seeding','AdminCrosspointsController@crossSeeding', function () {
  });
});

Route::group([
  'prefix'     => 'admin',
  'middleware' => ['web']
], function () {
  Route::post('telemetries-import','AdminTelemetriesController@import', function () {
  });
  Route::get('telemetries-seeding','AdminTelemetriesController@telemetriesSeeding', function () {
  });
});

Route::group([
  'prefix'     => 'admin',
  'middleware' => ['web']
], function () {
  Route::post('cpoints-import','AdminCpointsController@import', function () {
  });
  Route::get('cpoints-seeding','AdminCpointsController@cPointsSeeding', function () {
  });
});

Route::group([
  'prefix'     => 'admin',
  'middleware' => ['web']
], function () {
  Route::post('avcclusters-import','AdminAvcclustersController@import', function () {
  });
  Route::get('avcclusters-seeding','AdminAvcclustersController@avcClustersSeeding', function () {
  });
});

Route::get('test',function() {
  phpinfo();
});


