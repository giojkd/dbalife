<?php

use Illuminate\Http\Request;
use App\Http\Middleware\Cors;
use App\libraries\lib\WonderPush;
use App\libraries\lib\WonderPush\NotificationAlert;

use App\libraries\lib\WonderPush\Api;
use App\libraries\lib\WonderPush\Errors;
use App\libraries\lib\WonderPush\Net;
use App\libraries\lib\WonderPush\Obj;
use App\libraries\lib\WonderPush\RequestBuilders;
use App\libraries\lib\WonderPush\ResponseParsers;
use App\libraries\lib\WonderPush\Util;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test-log',function(){
  Log::info('A user has arrived at the welcome page.');
  echo 'test';
})
->middleware(Cors::class);




Route::get('/user', function (Request $request) {
  return $request->user();
})->middleware('auth:api');


Route::get('/text-update','ApiText@textUpdate')
->middleware(Cors::class);

Route::get('/contact-us','ApiContacts@newContact')
->middleware(Cors::class);

Route::get('/alife-user','ApiUser@user')
->middleware(Cors::class);

Route::get('/user-update','ApiUser@userUpdate')
->middleware(Cors::class);

Route::get('/forgot','ApiUser@forgot')
->middleware(Cors::class);

Route::get('/users-ranking/','ApiUser@usersRanking')
->middleware(Cors::class);

Route::get('/sign-in','ApiUser@signin')
->middleware(Cors::class);

Route::get('/sign-up','ApiUser@signup')
->middleware(Cors::class);

Route::post('/file-upload','ApiFile@upload')
->middleware(Cors::class);

Route::get('/animals','ApiAnimal@listAnimals')
->middleware(Cors::class);

Route::post('/report','ApiReport@newReport')
->middleware(Cors::class);

Route::post('/report-update','ApiReport@updateReport' )
->middleware(Cors::class);

Route::post('/report-import','ApiReport@import', function () {
})
->middleware(Cors::class);

Route::get('/report-delete','ApiReport@delReport')
->middleware(Cors::class);

Route::get('/reports','ApiReport@getReports')
->middleware(Cors::class);

Route::get('/reports-seeding','ApiSeeders@reportSeeding')
->middleware(Cors::class);

Route::get('/cross-seeding','ApiSeeders@crossSeeding')
->middleware(Cors::class);

Route::get('/telemetry-seeding','ApiSeeders@telemetrySeeding')
->middleware(Cors::class);

Route::get('/test-push',function(){

  include_once(app_path() . '/libraries/wonderpush/init.php');

  $wonderpush = new \WonderPush\WonderPush('NmFiYjI0YThjYzI5OTk1MDJhMThmODhkY2IwNWJkN2YyNWJhZGJjMmQ4OTAyMGEzOGM0NDY4OWNkYTUwYjEyYw', '01dl4oh7o97g7a32');




  $response = $wonderpush->deliveries()->prepareCreate()
  ->setTargetSegmentIds('@ALL')
  ->setNotification(
    \WonderPush\Obj\Notification::_new()
    ->setAlert(
        \WonderPush\Obj\NotificationAlert::_new()
        ->setTitle('Using the PHP library')
        ->setText('Hello, WonderPush!')
      )
    )
    ->execute()
    ->checked();

    dd($response);

    })
    ->middleware(Cors::class);
