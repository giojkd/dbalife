<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //

    public function model()
   {
       return $this->morphTo();
   }
}
