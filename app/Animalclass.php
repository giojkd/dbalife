<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animalclass extends Model
{
  public function animals()
  {
    return $this->hasMany('App\Animal');
  }
  public function texts(){
    return $this->morphMany('App\Text','model');
  }
}
