<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
  protected $guarded = [];

 
  public function animal()
  {
    return $this->belongsTo('App\Animal','animal_id','id');
  }
  public function type(){
    return $this->belongsTo('App\Reporttype','reporttype_id','id');
  }
  public function files(){
    return $this->morphMany('App\File','model');
  }

  
}
