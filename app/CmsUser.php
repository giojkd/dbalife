<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsUser extends Model
{
     protected $table = 'cms_users';
     protected $guarded = [];


     public function reports(){
       return $this->hasMany('App\Report');
     }

     public function countReports(){
       return count($this->reports());
     }

}
