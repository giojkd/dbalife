<?php namespace App\Http\Controllers;

use Session;
use App\Report;
use App\Animal;
use App\Avccluster;
use App\Avcps;
use App\CmsUser;
use App\Cpoint;
use App\Reporttype;
use App\Crosspoint;
use App\Dataprivilege;
use App\Panel;
use App\Telemetry;
use App\User;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;




class AdminReportsController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		$profile = DB::table('cms_users')->where('id',CRUDBooster::myId())->first();


		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = true;
		$this->table = "reports";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"ID","name"=>"id"];
		$this->col[] = ["label"=>"Species","name"=>"animal_id","join"=>"animals,scientific_name"];
		$this->col[] = ["label"=>"Type","name"=>"reporttype_id","join"=>"reporttypes,name"];
		$this->col[] = ['label'=>'Lat','name'=>'lat'];
		$this->col[] = ['label'=>'Lng','name'=>'lng'];
		$this->col[] = ['label'=>'Date','name'=>'created_at','callback_php'=>'date("d/m/Y",strtotime($row->created_at))'];
		$this->col[] = ["label"=>"Notes","name"=>"notes"];
		$this->col[] = ["label"=>"Reliable","name"=>"reliable"];
		$this->col[] = ["label"=>"Enabled","name"=>"enabled"];
		$this->col[] = ["label"=>"User","name"=>"cms_user_id","join"=>"cms_users,name"];
		$this->col[] = ["label"=>"Source","name"=>"report_source"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		
		$this->form[] = ['label'=>'Species','name'=>'animal_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'animals,name'];
		$this->form[] = ['label'=>'Report Type','name'=>'reporttype_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'reporttypes,name'];
		$this->form[] = ['label'=>'Notes','name'=>'notes','type'=>'textarea','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Lat','name'=>'lat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Lng','name'=>'lng','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>"Reliable","name"=>"reliable",'type'=>'checkbox','dataenum'=>'1|yes'];
		$this->form[] = ['label'=>"Enabled","name"=>"enabled",'type'=>'checkbox','dataenum'=>'1|yes'];
		$this->form[] = ['label'=>'created_at','name'=>'created_at','type'=>'hidden','value'=>now()];
		$this->form[] = ['label'=>'updated_at','name'=>'updated_at','type'=>'hidden','value'=>now()];
		if(CRUDBooster::isSuperadmin()) {
			$this->form[] = ['label'=>'User','name'=>'cms_user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Source','name'=>'report_source','type'=>'select','dataenum'=>'App;Portal'];
		} else {
			$this->form[] = ['label'=>'User','name'=>'cms_user_id','type'=>'hidden','value'=>CRUDBooster::myID()];
			$this->form[] = ['label'=>'Source','name'=>'report_source','type'=>'hidden','value'=>$profile->user_source];
		}

		//$reportRows[] = ['label'=>'Animal','name'=>'animal_id','type'=>'datamodal','datamodal_table'=>'animals','datamodal_columns'=>'name','datamodal_where'=>'','datamodal_size'=>'small'];
		//$this->form[] = ['label'=>'Report rows','name'=>'report_row','type'=>'child','columns'=>$reportRows,'table'=>'reportrows','foreign_key'=>'report_id'];

		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ["label"=>"Reporttype Id","name"=>"reporttype_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"reporttype,id"];
		//$this->form[] = ["label"=>"Lat","name"=>"lat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Lng","name"=>"lng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Cms User Id","name"=>"cms_user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_user,id"];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();
		if(CRUDBooster::isSuperadmin()) {
			$this->index_button[] = ['label'=>'Import','url'=>'/admin/reports/reportfile','icon'=>'fa fa-download'];
		}

		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		$this->pre_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}


	/*
	| ----------------------------------------------------------------------
	| Hook for button selected
	| ----------------------------------------------------------------------
	| @id_selected = the id selected
	| @button_name = the name of button
	|
	*/
	public function actionButtonSelected($id_selected,$button_name) {
		//Your code here

	}


	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate query of index result
	| ----------------------------------------------------------------------
	| @query = current sql query
	|
	*/
	public function hook_query_index(&$query) {
		if(!CRUDBooster::isSuperadmin()) {
			//$arrpriv = Dataprivilege::select('guest_id')->where('owner_id',CRUDBooster::myID())->get()->toArray();
			//dd($arrpriv);
			$query->where('cms_user_id',CRUDBooster::myID())->orWhere('report_source','App');
			/*foreach($arrpriv as $priv) {
				$query->orWhere('cms_user_id',$priv);
			}*/
		}
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate row of index table html
	| ----------------------------------------------------------------------
	|
	*/
	public function hook_row_index($column_index,&$column_value) {
		//Your code here
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before add data is execute
	| ----------------------------------------------------------------------
	| @arr
	|
	*/
	public function hook_before_add(&$postdata) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after add public static function called
	| ----------------------------------------------------------------------
	| @id = last insert id
	|
	*/
	public function hook_after_add($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before update data is execute
	| ----------------------------------------------------------------------
	| @postdata = input post data
	| @id       = current id
	|
	*/
	public function hook_before_edit(&$postdata,$id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after edit public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_edit($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command before delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_before_delete($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_delete($id) {
		//Your code here

	}

	public function getDetail($id) {
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}

		$report = Report::with(['animal','type'])->find($id);

		#dd($reports);

		$return = [
			'id' => $report->id,
			'created_at' => $report->created_at->format('d/m/Y H:i'),
			'animal' => $report->animal->scientific_name,#texts()->where('text_type','name')->where('language_id',1)->first()->value,
			'lat' => $report->lat,
			'lng' => $report->lng,
			'notes' => $report->notes,
			'type' => $report->type->name
		];

	
		if(isset($report->files)){
			foreach($report->files as $file){
				$return['files'][] = implode('/',[env('APP_URL'),$file->path]);
			}
		}

		

		$data = $return;
		//dd($data);
		//Please use cbView method instead view method from laravel
		$this->cbView('custom_report_detail_view',$data);
	}

	public function getReportfile() {
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}
		$data = [];


		$this->cbView('custom_report_import_view',$data);
	}

	public function getMap() {
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}

		$data = [];
		$data['animals'] = Animal::get();
		$data['sources'] = [];
		$arrpriv = new Collection();
		$idpriv = []; 
		$idpriv[] = CRUDBooster::myID();
		if(!CRUDBooster::isSuperadmin()) {
			$arrpriv = Dataprivilege::select('guest_id')->where('owner_id',CRUDBooster::myID())->get()->toArray();
			$query = Report::select('report_source')
			->where('cms_user_id',CRUDBooster::myID())
			->orWhere('report_source','App')
			->groupBy('report_source');
			foreach($arrpriv as $priv) {
				$query->orWhere('cms_user_id',$priv);
			}
			$data['sources'] = $query->get()->toArray();
		} else {
			$data['sources'] = Report::select('report_source')
			->groupBy('report_source')
			->get()
			->toArray();
		}
		foreach($arrpriv as $pr) {
			$idpriv[] = $pr['guest_id'];
		}
	
		$data['types'] = Reporttype::get();
		$crosses = null;
		if(!CRUDBooster::isSuperadmin()) {
			$crosses = Crosspoint::whereIn('cms_user_id',$idpriv)->get();
		} else {
			$crosses = Crosspoint::get();
		}
		$reports = Report::with(['animal','type'])->get();

		$telemetries = null;
		if(!CRUDBooster::isSuperadmin()) {
			$telemetries = Telemetry::whereIn('cms_user_id',$idpriv)->get();
		} else {
			$telemetries = Telemetry::get();
		}

		$cpoints = null;
		$partners = null;
		if(!CRUDBooster::isSuperadmin()) {
			$partners = CmsUser::where('id_cms_privileges',3)->whereIn('id',$idpriv)->get();
			foreach($partners as $user) {
				$cpoints[$user->id] = Cpoint::where('cms_user_id',$user->id)->get();
			}
		} else {
			$partners = CmsUser::where('id_cms_privileges',3)->get();
			foreach($partners as $user) {
				$cpoints[$user->id] = Cpoint::where('cms_user_id',$user->id)->get();
			}
			
		}
		
		$avcclusters = null;
		if(!CRUDBooster::isSuperadmin()) {
			$avcclusters = Avccluster::whereIn('cms_user_id',$idpriv)->get();
		} else {
			$avcclusters = Avccluster::get();
		}

		$panels = null;
		if(!CRUDBooster::isSuperadmin()) {
			$panels = Panel::whereIn('cms_user_id',$idpriv)->get();
		} else {
			$panels = Panel::get();
		}

		$avcpss = null;
		if(!CRUDBooster::isSuperadmin()) {
			$avcpss = Avcps::whereIn('cms_user_id',$idpriv)->get();
		} else {
			$avcpss = Avcps::get();
		}
		
		foreach ($partners->pluck('name','id')  as $key => $partner) {
			$data['partners'][$key] = $partner;
		}
		
		foreach($reports as $report){
			$data['reports'][] = [
						'id' => $report->id,
						'created_at' => $report->created_at->format('d/m/Y H:i'),
						'animal' => $report->animal->scientific_name,#texts()->where('text_type','name')->where('language_id',1)->first()->value,
						'lat' => $report->lat,
						'lng' => $report->lng,
						'notes' => $report->notes,
						'type' => $report->type->name,
						'kind' => 'animal',
						'source' => $report->report_source,
			];
		}
		foreach($telemetries as $telemetry){
			$data['telemetries'][] = [
						'id' => $telemetry->id,
						'created_at' => $telemetry->created_at->format('d/m/Y H:i'),
						'year' => $telemetry->year,
						'lat_start' => $telemetry->Y_start_li,
						'lat_end' => $telemetry->Y_final_li,
						'lng_start' => $telemetry->X_start_li,
						'lng_end' => $telemetry->X_final_li,
						'kind' => 'telemetry',
						'animal' => $telemetry->latin_name,
						'source' => $telemetry->telemetry_source,
			];
		}
		foreach($crosses as $cross){
			$data['crosses'][] = [
						'id' => $cross->id,
						'created_at' => $cross->created_at->format('d/m/Y H:i'),
						'lat' => $cross->lat,
						'lng' => $cross->lng,
						'notes' => $cross->notes,
						'type' => $cross->cross_type,
						'kind' => 'cross',
						'source' => $cross->cross_source,
			];
		}
		foreach($cpoints as $key => $cpointpartners) {
			foreach($cpointpartners as $cpoint){
				$data['cpoints'][$key][] = [
					'id' => $cpoint->id,
					'created_at' => $cpoint->created_at->format('d/m/Y H:i'),
					'year' => $cpoint->year,
					'lat' => $cpoint->lat,
					'lng' => $cpoint->lng,
					'kind' => 'cpoint_'.$key,
					'animal' => $cpoint->latin_name,
					'source' => $cpoint->cpoint_source,
				];
			}
		}
		//dd($data['cpoints']);
		

		foreach($avcclusters as $avccluster){
			$data['avcclusters'][] = [
						'id' => $avccluster->id,
						'created_at' => $avccluster->created_at->format('d/m/Y H:i'),
						'year' => $avccluster->year,
						'lat_start' => $avccluster->Y_start_li,
						'lat_end' => $avccluster->Y_final_li,
						'lng_start' => $avccluster->X_start_li,
						'lng_end' => $avccluster->X_final_li,
						'kind' => 'avccluster',
						'source' => $avccluster->avccluster_source,
			];
		}

		foreach($panels as $panel){
			$data['panels'][] = [
						'id' => $panel->id,
						'created_at' => $panel->created_at->format('d/m/Y H:i'),
						'lat' => $panel->lat,
						'lng' => $panel->lng,
						'type' => $panel->panel_type,
						'kind' => 'panel',
						'source' => $panel->data_source,
			];
		}

		foreach($avcpss as $avcps){
			$data['avcpss'][] = [
						'id' => $avcps->id,
						'created_at' => $avcps->created_at->format('d/m/Y H:i'),
						'lat' => $avcps->lat,
						'lng' => $avcps->lng,
						'type' => $avcps->componet,
						'kind' => 'avcps',
						'source' => $avcps->data_source,
			];
		}

		//dd($data);
		//$data = $return;

		//Please use cbView method instead view method from laravel
		$this->cbView('custom_report_map_view',$data);
	}

	public function import(Request $request) {
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}
		$data = $request->all();
		if ($request->hasFile('importing-file')) {
			$file = $request->file('importing-file');
			if ($file->isValid()) {
				Storage::disk('local')->putFileAs('public/report', $file, 'file.csv');
			}
		}
		return back()->with('success','Uploads succedeed');
	}

	public function reportSeeding(Request $request){
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}
		//$data = $request->all();
		$results = Excel::load('uploads/public/report/file.csv', function($reader)  {})->toArray();
		foreach($results as $data) {
		  $upd = date('Y-m-d', strtotime($data['date']));
		  $animal = DB::table('animals')->where('scientific_name', $data['latin_name'])->first();
		  $user = DB::table('cms_users')->where('user_source',$data['partners'])->first();
		  $userid = env('DEFAULT_USER');
		  if ($user) {
			$userid = $user->id;
		  }
		  $gender = $data['sex'];
		  if ($gender != 'M' && $gender != 'F') {
			$gender = '?';
		  }
		  $report = new Report;
		  $report->lat =  $data['y'];
		  $report->lng=  $data['x'];
		  $report->extid = $data['fid'];
		  $report->created_at = $upd;
		  $report->updated_at = $upd;
		  $report->reporttype_id = 1;
		  $report->animal_id = $animal->id;
		  $report->cms_user_id = $userid;
		  $report->notes = $data['note'];
		  $report->report_source = $data['partners'];
		  $report->gender = $gender;
		  $report->age = $data['age'];
		  //dump($report);
		  $report->save();
		}
		Storage::disk('local')->move('public/report/file.csv', 'public/report/file_'.time().'.csv');
		return back()->with('success','Import succedeed');
	  }



	//By the way, you can still create your own method in here... :)


}
