<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use App\Crosspoint;
use App\Telemetry;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ApiSeeders extends Controller
{
    public function reportSeeding(Request $request){
      //$data = $request->all();

      $results = Excel::load('uploads/public/report/file.csv', function($reader)  {})->toArray();
      foreach($results as $data) {
        $upd = date('Y-m-d', strtotime($data['date']));
        $animal = DB::table('animals')->where('scientific_name', $data['latin_name'])->first();
        $user = DB::table('cms_users')->where('user_source',$data['partners'])->first();
        $userid = env('DEFAULT_USER');
        if ($user) {
          $userid = $user->id;
        }
        $gender = $data['sex'];
        if ($gender != 'M' && $gender != 'F') {
          $gender = '?';
        }
        $report = new Report;
        $report->lat =  $data['y'];
        $report->lng=  $data['x'];
        $report->extid = $data['fid'];
        $report->created_at = $upd;
        $report->updated_at = $upd;
        $report->reporttype_id = 1;
        $report->animal_id = $animal->id;
        $report->cms_user_id = $userid;
        $report->notes = $data['note'];
        $report->report_source = $data['partners'];
        $report->gender = $gender;
        $report->age = $data['age'];
        //dump($report);
        $report->save();
      }
      return $results;
    }

    public function crossSeeding(Request $request){
      //$data = $request->all();

      $results = Excel::load('uploads/public/cross/file.csv', function($reader) {})->toArray();
      foreach($results as $data) {
        $upd = date('Y-m-d', strtotime($data['date_inspection']));
        $user = DB::table('cms_users')->where('user_source',$data['partners'])->first();
        $userid = env('DEFAULT_USER');
        if ($user) {
          $userid = $user->id;
        }
        if (!is_numeric($data['coordinates_utm_y'])) {
          continue;
        }
        $lat = str_replace(",",".",$data['coordinates_utm_x']);
        $lng = str_replace(",",".",$data['coordinates_utm_y']);
        if (strpos($lat, '.') === false) {
          $lat = substr_replace($lat,".",2,0);
        }
        if (strpos($lng, '.') === false) {
          $lng = substr_replace($lng,".",2,0);
        }
        $report = new Crosspoint;
        $report->lat =  $lat;
        $report->lng=  $lng;
        $report->extid = $data['id'];
        $report->created_at = $upd;
        $report->updated_at = $upd;
        $report->cross_type = $data['type_of_crossing_structure'];
        $report->cross_use = $data['uses_of_the_passages'];
        $report->cms_user_id = $userid;
        $report->notes = $data['note'];
        $report->cross_observations = $data['observations'];
        $report->cross_source = $data['partners'];
        $report->structure_code = $data['structure_code'];
        $report->road_code = $data['road_code'];
        //dump($report);
        $report->save();
      }
      return $results;
    }

    public function telemetrySeeding(Request $request){
      //$data = $request->all();

      $results = Excel::load('uploads/public/telemetry/file.csv', function($reader) {})->toArray();
      foreach($results as $data) {
        $user = DB::table('cms_users')->where('user_source',$data['partners'])->first();
        $userid = env('DEFAULT_USER');
        if ($user) {
          $userid = $user->id;
        }
        
        $report = new Telemetry;
        $report->fid = $data['fid'];
        $report->shape = $data['shape'];
        $report->id_clus = $data['id_clus'];
        $report->id_line = $data['id_line'];
        $report->npts_clus = $data['npts_clus'];
        $report->npts_line = $data['npts_line'];
        $report->strength = $this->transcodeNumericFormat($data['strength']);
        $report->str_ci_1 = $this->transcodeNumericFormat($data['str_ci_1']);
        $report->str_ci_2 = $this->transcodeNumericFormat($data['str_ci_2']);
        $report->clus_from = $this->transcodeNumericFormat($data['clus_from']);
        $report->clus_to = $this->transcodeNumericFormat($data['clus_to']);
        $report->len_clus = $this->transcodeNumericFormat($data['len_clus']);
        $report->len_line = $this->transcodeNumericFormat($data['len_line']);
        $report->dens_point = $this->transcodeNumericFormat($data['dens_point']);
        $report->str_dens2 = $this->transcodeNumericFormat($data['str_dens2']);
        $report->sd2_ci_1 = $this->transcodeNumericFormat($data['sd2_ci_1']);
        $report->sd2_ci_2 = $this->transcodeNumericFormat($data['sd2_ci_2']);
        $report->gstr = $this->transcodeNumericFormat($data['gstr']);
        $report->gstr_ci_1 = $this->transcodeNumericFormat($data['gstr_ci_1']);
        $report->gstr_ci_2 = $this->transcodeNumericFormat($data['gstr_ci_2']);
        $report->cms_user_id = $userid;
        $report->telemetry_source = $data['partners'];
        $report->year = $data['data'];
        $report->extid = $data['id_part'];
        //dump($report);
        $report->save();
      }
      return $results;
    }

    public function transcodeNumericFormat(String $field) {
      $rt = "";
      if ($field != '') {
        $rt = str_replace(",",".",$field);
      }
      return $rt;
    }
}