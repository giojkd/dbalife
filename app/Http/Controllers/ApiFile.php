<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Storage;




class ApiFile extends Controller
{
  public function upload(Request $request){


    $data = $request->all();
    $imageData = $data['imageData'];


    $file = new File;
    $file->path = '';
    $file->model_type = 'App\Report';
    $file->save();

    $fileToStore = base64_decode($imageData);

    $fileName = "image-".$file->id.".jpg";

    Storage::disk('local')->put($fileName, $fileToStore);

    $file->path = 'uploads/'.$fileName;
    $file->save();
    
    return [
      'id' => $file->id
    ];
  }
}
