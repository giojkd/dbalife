<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use App\Reportrow;
use App\File;
use Illuminate\Support\Facades\Log;
use App\Language;

class ApiReport extends Controller
{



  public function getReports(Request $request){
     $data = $request->all();
     $sw = $data['southwest'];
     $ne = $data['northeast'];
     $with = ['animal','type'];
     if(isset($data['id'])){
       if(\is_numeric($data['id']) && (int) $data['id'] > 0){

         #$with[] = 'photos';
       }
     }

    $reports = Report::with($with);

     if(isset($data['id'])){
       if(\is_numeric($data['id']) && (int) $data['id'] > 0){
         $reports->where('id',$data['id']);

       }
     }

     //$reports = $reports->where('report_source','App')->where('cms_user_id', $data['user'])->get();
     $reports = $reports->where('report_source','App')->where('enabled',true)->get();
     #dd($reports);
     $lang = Language::where('slug',$data['lang'])->first();
     $langId = $lang->id;

     foreach($reports as $report){
       $files = [];
       if(isset($report->files)){
         foreach($report->files as $file){
           $files[] = implode('/',[env('APP_URL'),$file->path]);
         }
       }
       $return[] = [
         'id' => $report->id,
         'created_at' => $report->created_at->format('d/m/Y H:i'),
         'animal' => $report->animal->texts()->where('text_type','name')->where('language_id',$langId)->first()->value,#$report->rows[0]->animal->name,
         //'animal' => $report->animal->name,
         'lat' => $report->lat,
         'lng' => $report->lng,
         'cms_user_id' => $report->cms_user_id,
         'notes' => $report->notes,
         'reliable' => $report->reliable,
         'type' => $report->type->name,
         'photos' =>$files
       ];
     }
     return($return);

  }

  public function delReport(Request $request) {
    $data = $request->all();
    $hash = 'HFGRTTY';
    Log::info($data);
    $report = Report::where('id',$data['id'])->where('cms_user_id',$data['cms_user_id'])->first();
    if ($data['hash'] != $hash || !$report) { 
      $return = [];
      $return['status'] = 0;
      $return['report_id'] = $data['id'];
      return $return;
    }
    $report->enabled = false;
    $report->save();
    $return = [];
    $return['status'] = 1;
    $return['report_id'] = $data['id'];
    return $return;
  }

  public function updateReport(Request $request) {
    $data = $request->all();
    $hash = 'HFGRTTY';
    Log::info($data);
    $reliable = 0;
    if ($data['report']['reliable']) {
      $reliable = 1;
    }
    $report = Report::where('id',$data['id'])->where('cms_user_id',$data['cms_user_id'])->first();
    if ($data['hash'] != $hash || !$report) { 
      $return = [];
      $return['status'] = 0;
      $return['message'] = 'error';
      return $return;
    }
    $report->reliable = $reliable;
    $report->save();
    $return = [];
    $return['status'] = 1;
    $return['report_id'] = $data['id'];
    return $return;
  }

  public function newReport(Request $request){
    $data = $request->all();
    Log::info($data);
    $report = new Report;
    $reliable = 0;
    if ($data['report']['reliable']) {
      $reliable = 1;
    }
    $report->lat =  $data['lat'];
    $report->lng=  $data['lng'];
    $report->reporttype_id = $data['report']['reporttype_id'];
    $report->animal_id = $data['report_row']['animal_id'];
    $report->cms_user_id = $data['report']['cms_user_id'];
    $report->notes = $data['report']['notes'];
    $report->reliable = $reliable;
    $report->save();
    if(isset($data['files'])){
      if($data['files'] != ''){
        $filesList = File::whereIn('id',explode(',',$data['files']))->get();
        foreach($filesList as $file){
          $file->model_id = $report->id;
          $file->save();
        }
      }
    }

    $return = [];
    $return['status'] = 1;
    $report['report_id'] = $report->id;
    return $return;
  }

  
}
