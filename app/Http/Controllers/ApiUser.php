<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\CmsUser;
use CRUDbooster;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;


class ApiUser extends Controller
{

  public function usersRanking(Request $request){
    $users = CmsUser::withCount('reports')
      ->whereNotIn('id_cms_privileges', [1,3])
      ->orWhereNull('id_cms_privileges')
      ->having('reports_count' , '>', 0)
      ->orderBy('reports_count','desc')
      ->get();
    return $users;
  }

  public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
 }

  public function forgot(Request $request){
    $data = $request->all();
    $email = strtolower(trim($data['email']));
    $CmsUser = \App\CmsUser::where('email',$email)->first();

    if(is_null($CmsUser)){
      return [
        'status' =>  0,
        'message' => 'email_not_present'
      ];
    } else {
      $newpass = $this->generateRandomString(6);
      $data['password'] = Hash::make($newpass);
      $newdata['password'] = $newpass;
      $CmsUser->fill($data);
      $CmsUser->save();
      $sendEmailCfg = ['to'=>$email,'data'=>$newdata,'template'=>'forgot_password_backend'];
      CRUDBooster::sendEmail($sendEmailCfg);
      return [
              'status' =>  1,
              'message' => 'pass_sended'
            ];
    }
  }

  public function userUpdate(Request $request){
    $data = $request->all();


    $userPost = $data['user'];

    if(trim($userPost['password']) != ''){
        if(trim($userPost['password']) == trim($userPost['repeat_password'])){
              unset($userPost['repeat_password']);
              $userPost['password'] = Hash::make($userPost['password']);
        }else{
            Log::info('Passwords do not match');
          return ['status' =>  0,'message' => 'Password verification does not match'];
        }
    }else{
      unset($userPost['password']);
      unset($userPost['repeat_password']);
    }

    $user = CmsUser::find($data['id']);

    $user->fill($userPost);
    $user->save();

    return ['status' =>  1];
  }

  public function user(Request $request){
    $user = CmsUser::find(1);
    return $user;
  }

  public function signup(Request $request){
    $data = $request->all();
    $validateData = $this->validate($request,[
      'email' => 'required|email',
      'name' => 'required',
      'surname' => 'required',
      'password' => 'required',
      'repeat_password' => 'required',
    ]);

    $CmsUser = \App\CmsUser::where('email',$data['email'])->first();

    if(!is_null($CmsUser)){
      return response([], 423);
    }

    if($data['password'] != $data['repeat_password']){
      return response([], 424);
    }

    unset($data['repeat_password']);

    $data['password'] = Hash::make($data['password']);

    $CmsUser = \App\CmsUser::create($data);

    return $data;

  }

  public function signin(Request $request){
    $data = $request->all();
    $email = strtolower(trim($data['email']));
    $password = $data['password'];
    $CmsUser = \App\CmsUser::where('email',$email)->first();

    if(is_null($CmsUser)){
      return [
        'status' => 0,
        'err_message' => 'There is no user matching this email',
      ];
    }else{
      if (Hash::check($password, $CmsUser->password)) {
        return [
          'status' => 1,
          'data' => $CmsUser
        ];
      }else{
        return [
          'status' => 0,
          'err_message' => 'The password does not match email '.$email,
        ];
      }
    }
  }

}
