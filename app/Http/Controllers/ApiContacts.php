<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CRUDbooster;
use Illuminate\Support\Facades\Log;

class ApiContacts extends Controller
{
  public function newContact(Request $request){
    $data = $request->all();
    Log::info($data);
    $sendEmailCfg = ['to'=>env('CONTACT_US_EMAIL_RECIPIENT'),'data'=>$data,'template'=>'contact_us'];
    Log::info($sendEmailCfg);
    CRUDBooster::sendEmail($sendEmailCfg);
    $data['status'] = 1;
    return $data;
  }
}
