<?php namespace App\Http\Controllers;

use App\Avccluster;
use Session;
use Illuminate\Http\Request;

	use CRUDBooster;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class AdminAvcclustersController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "avcclusters";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Avccluster Source","name"=>"avccluster_source"];
			$this->col[] = ["label"=>"Clus From","name"=>"clus_from"];
			$this->col[] = ["label"=>"Clus To","name"=>"clus_to"];
			$this->col[] = ["label"=>"User Id","name"=>"cms_user_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Dens Point","name"=>"dens_point"];
			$this->col[] = ["label"=>"Enabled","name"=>"enabled"];
			$this->col[] = ["label"=>"Extid","name"=>"extid"];
			$this->col[] = ["label"=>"Fid","name"=>"fid"];
			$this->col[] = ["label"=>"Gstr","name"=>"gstr"];
			$this->col[] = ["label"=>"Gstr Ci_1","name"=>"gstr_ci_1"];
			$this->col[] = ["label"=>"Gstr Ci_2","name"=>"gstr_ci_2"];
			$this->col[] = ["label"=>"Clus","name"=>"id_clus"];
			$this->col[] = ["label"=>"Line","name"=>"id_line"];
			$this->col[] = ["label"=>"Len Clus","name"=>"len_clus"];
			$this->col[] = ["label"=>"Len Line","name"=>"len_line"];
			$this->col[] = ["label"=>"Npts Clus","name"=>"npts_clus"];
			$this->col[] = ["label"=>"Npts Line","name"=>"npts_line"];
			$this->col[] = ["label"=>"Sd2 Ci_1","name"=>"sd2_ci_1"];
			$this->col[] = ["label"=>"Sd2 Ci_2","name"=>"sd2_ci_2"];
			$this->col[] = ["label"=>"Shape","name"=>"shape"];
			$this->col[] = ["label"=>"Str Ci_1","name"=>"str_ci_1"];
			$this->col[] = ["label"=>"Str Ci_2","name"=>"str_ci_2"];
			$this->col[] = ["label"=>"Str Dens2","name"=>"str_dens2"];
			$this->col[] = ["label"=>"Strength","name"=>"strength"];
			$this->col[] = ["label"=>"X Final_li","name"=>"X_final_li"];
			$this->col[] = ["label"=>"X Start_li","name"=>"X_start_li"];
			$this->col[] = ["label"=>"Y Final_li","name"=>"Y_final_li"];
			$this->col[] = ["label"=>"Y Start_li","name"=>"Y_start_li"];
			$this->col[] = ["label"=>"Year","name"=>"year"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Avccluster Source','name'=>'avccluster_source','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Clus From','name'=>'clus_from','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Clus To','name'=>'clus_to','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Cms User Id','name'=>'cms_user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Dens Point','name'=>'dens_point','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Enabled','name'=>'enabled','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Extid','name'=>'extid','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Fid','name'=>'fid','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gstr','name'=>'gstr','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gstr Ci 1','name'=>'gstr_ci_1','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gstr Ci 2','name'=>'gstr_ci_2','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
		
			$this->form[] = ['label'=>'Len Clus','name'=>'len_clus','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Len Line','name'=>'len_line','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Npts Clus','name'=>'npts_clus','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Npts Line','name'=>'npts_line','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sd2 Ci 1','name'=>'sd2_ci_1','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sd2 Ci 2','name'=>'sd2_ci_2','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Shape','name'=>'shape','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Str Ci 1','name'=>'str_ci_1','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Str Ci 2','name'=>'str_ci_2','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Str Dens2','name'=>'str_dens2','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Strength','name'=>'strength','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'X Final Li','name'=>'X_final_li','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'X Start Li','name'=>'X_start_li','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Y Final Li','name'=>'Y_final_li','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Y Start Li','name'=>'Y_start_li','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Year','name'=>'year','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Avccluster Source","name"=>"avccluster_source","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Clus From","name"=>"clus_from","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Clus To","name"=>"clus_to","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Cms User Id","name"=>"cms_user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_user,id"];
			//$this->form[] = ["label"=>"Dens Point","name"=>"dens_point","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Enabled","name"=>"enabled","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Extid","name"=>"extid","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Fid","name"=>"fid","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Gstr","name"=>"gstr","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Gstr Ci 1","name"=>"gstr_ci_1","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Gstr Ci 2","name"=>"gstr_ci_2","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Clus","name"=>"id_clus","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"clus,id"];
			//$this->form[] = ["label"=>"Line","name"=>"id_line","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"line,id"];
			//$this->form[] = ["label"=>"Len Clus","name"=>"len_clus","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Len Line","name"=>"len_line","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Npts Clus","name"=>"npts_clus","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Npts Line","name"=>"npts_line","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Sd2 Ci 1","name"=>"sd2_ci_1","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Sd2 Ci 2","name"=>"sd2_ci_2","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Shape","name"=>"shape","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Str Ci 1","name"=>"str_ci_1","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Str Ci 2","name"=>"str_ci_2","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Str Dens2","name"=>"str_dens2","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Strength","name"=>"strength","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"X Final Li","name"=>"X_final_li","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"X Start Li","name"=>"X_start_li","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Y Final Li","name"=>"Y_final_li","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Y Start Li","name"=>"Y_start_li","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Year","name"=>"year","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
			$this->index_button = array();
			if(CRUDBooster::isSuperadmin()) {
				$this->index_button[] = ['label'=>'Import','url'=>'/admin/avcclusters/avcclustersfile','icon'=>'fa fa-download'];
			}



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        if(!CRUDBooster::isSuperadmin()) {
				$query->where('cms_user_id',CRUDBooster::myID());
			}
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

		public function getAvcclustersfile() {
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$data = [];
	
	
			$this->cbView('custom_avccluster_import_view',$data);
		}

		public function import(Request $request) {
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$data = $request->all();
			if ($request->hasFile('importing-file')) {
				$file = $request->file('importing-file');
				if ($file->isValid()) {
					Storage::disk('local')->putFileAs('public/avcluster', $file, 'file.csv');
				}
			}
			return back()->with('success','Uploads succedeed');
		}

		public function avcClustersSeeding(Request $request){
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			//$data = $request->all();
			$results = Excel::load('uploads/public/avcluster/file.csv', function($reader)  {})->toArray();
			foreach($results as $data) {
				$user = DB::table('cms_users')->where('user_source',$data['partners'])->first();
				$userid = env('DEFAULT_USER');
				if ($user) {
				  $userid = $user->id;
				}
				$report = new Avccluster();
				$report->fid = $data['fid'];
				$report->shape = $data['shape'];
				$report->id_clus = $data['id_clus'];
				$report->id_line = $data['id_line'];
				$report->npts_clus = $data['npts_clus'];
				$report->npts_line = $data['npts_line'];
				$report->strength = $this->transcodeNumericFormat($data['strength']);
				$report->clus_from = $this->transcodeNumericFormat($data['clus_from']);
				$report->clus_to = $this->transcodeNumericFormat($data['clus_to']);
				$report->len_clus = $this->transcodeNumericFormat($data['len_clus']);
				$report->len_line = $this->transcodeNumericFormat($data['len_line']);
				$report->dens_point = $this->transcodeNumericFormat($data['dens_point']);
				$report->str_dens2 = $this->transcodeNumericFormat($data['str_dens2']);

				$report->str_ci_1 = $this->transcodeNumericFormat($data['str_ci_1']);
				$report->str_ci_2 = $this->transcodeNumericFormat($data['str_ci_2']);
				$report->sd2_ci_1 = $this->transcodeNumericFormat($data['sd2_ci_1']);
				$report->sd2_ci_2 = $this->transcodeNumericFormat($data['sd2_ci_2']);
				$report->gstr_ci_1 = $this->transcodeNumericFormat($data['gstr_ci_1']);
				$report->gstr_ci_1 = $this->transcodeNumericFormat($data['gstr_ci_1']);

				$report->gstr = $this->transcodeNumericFormat($data['gstr']);
				$report->X_start_li = $this->coordsFormat($data['long_start_li']);
				$report->X_final_li = $this->coordsFormat($data['long_final_li']);
				$report->Y_start_li = $this->coordsFormat($data['lat_start_li']);
				$report->Y_final_li = $this->coordsFormat($data['lat_final_li']);
				$report->cms_user_id = $userid;
				$report->avccluster_source = $data['partners'];
				$report->year = $data['data'];
				$report->extid = $data['id_part'];
				//dump($data);
				$report->save();
			}
			Storage::disk('local')->move('public/avcluster/file.csv', 'public/avcluster/file_'.time().'.csv');
			return back()->with('success','Import succedeed');
		  }

		  public function transcodeNumericFormat($field) {
			$rt = "";
			if ($field != null && $field != '') {
			  $rt = str_replace(",",".",$field);
			}
			return $rt;
		  }

		  public function coordsFormat($field) {
			$rt = "";
			if ($field != null || $field != '') {
				$rt = str_replace(",",".",$field);
				if (strpos($rt, '.') === false) {
				  $rt = substr_replace($rt,".",2,0);
				}
			}
			return $rt;
		  }


	    //By the way, you can still create your own method in here... :) 


	}