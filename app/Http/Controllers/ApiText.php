<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Text;

class ApiText extends Controller
{
    public function textUpdate(Request $request){
      $data = $request->all();
      $text = Text::firstOrCreate([
        'language_id' => $data['language_id'],
        'model_id' => $data['model_id'],
        'model_type' => $data['model_type'],
        'text_type' => 'name'
      ]);
      $text->value = $data['value'];
      $text->save();
      return $data;
    }
}
