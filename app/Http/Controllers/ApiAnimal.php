<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animal;
use App\Animalclass;
use Illuminate\Support\Facades\Log;
use App\Language;

class ApiAnimal extends Controller
{
    public function listAnimals(Request $request){



      $data = $request->all();
      $lang = Language::where('slug',$data['lang'])->first();
      

      $langId = $lang->id;
      $defaultLanguage = 1;
      Log::info($data);
      $classes = Animalclass::with(['texts','animals','animals.texts'])->get();
   
      foreach($classes as $classIndex =>  $class){
        $animalClassTranslations = [];
        foreach($class->texts as $classText){
          $animalClassTranslations[$classText->language_id][$classText->text_type] = $classText->value;
        }

        $classesReturn[$classIndex]['id'] = $class->id;
        $classesReturn[$classIndex]['name'] = ($animalClassTranslations[$langId]['name'] != '') ? $animalClassTranslations[$langId]['name'] : $animalClassTranslations[$defaultLanguage]['name'];

        $animalsReturn = [];
        foreach($class->animals as $animalIndex => $animal){
          $animalTranslations = [];
          foreach($animal->texts as $animalText){
              $animalTranslations[$animalText->language_id][$animalText->text_type] = $animalText->value;
          }
          $animalsReturn[$animalIndex]['id'] = $animal->id;
          $animalsReturn[$animalIndex]['name'] = ($animalTranslations[$langId]['name'] != '') ? $animalTranslations[$langId]['name'] : $animalTranslations[$defaultLanguage]['name'];
        }
       
        $animalname = array_column($animalsReturn, 'name');
        array_multisort($animalname, SORT_ASC, $animalsReturn);
        $classesReturn[$classIndex]['animals'] = $animalsReturn;

        
      }
      return $classesReturn;
    }
}
