<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dataprivilege extends Model
{
  protected $guarded = [];

 
  public function owner()
  {
    return $this->belongsTo('App\CmsUser','owner_id','id');
  }
  public function guest()
  {
    return $this->belongsTo('App\CmsUser','guest_id','id');
  }
  
  
}
