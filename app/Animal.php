<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
  public function texts(){
    return $this->morphMany('App\Text','model');
  }
}
