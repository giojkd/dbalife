<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporttype extends Model
{
    protected $table = 'reporttypes';
}
