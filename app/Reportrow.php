<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportrow extends Model
{

  protected $guarded = [];
  public function report(){
    return $this->belongsTo('App\Report');
  }
  public function animal(){
    return $this->belongsTo('App\Animal');
  }
}
