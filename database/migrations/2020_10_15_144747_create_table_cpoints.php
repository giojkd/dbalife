<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCpoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fid')->nullable();
            $table->string('shape',255);
            $table->decimal('geom_lengt',11,8)->nullable();
            $table->decimal('speed',11,8)->nullable();
            $table->string('animal')->nullable();
            $table->integer('Filter')->nullable();
            $table->integer('year')->nullable();
            $table->integer('id_part')->nullable();
            $table->string('latin_name',255)->nullable();
            $table->decimal('lng',11,8)->nullable();
            $table->decimal('lat',11,8)->nullable();
            $table->string('cpoint_source',255);
            $table->integer('cms_user_id')->index();
            $table->integer('extid')->nullable();
            $table->boolean('enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpoints');
    }
}
