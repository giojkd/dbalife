<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAvcclusters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avcclusters', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fid')->nullable();
            $table->string('shape',255);
            $table->integer('id_clus')->nullable();
            $table->integer('id_line')->nullable();
            $table->integer('npts_clus')->nullable();
            $table->integer('npts_line')->nullable();
            $table->float('strength')->nullable();
            $table->float('str_ci_1')->nullable();
            $table->float('str_ci_2')->nullable();
            $table->float('clus_from')->nullable();
            $table->float('clus_to')->nullable();
            $table->float('len_clus')->nullable();
            $table->float('len_line')->nullable();
            $table->float('dens_point')->nullable();
            $table->float('str_dens2')->nullable();
            $table->float('sd2_ci_1')->nullable();
            $table->float('sd2_ci_2')->nullable();
            $table->float('gstr')->nullable();
            $table->float('gstr_ci_1')->nullable();
            $table->float('gstr_ci_2')->nullable();
            $table->integer('year')->nullable();
            $table->decimal('X_start_li', 11, 8)->nullable();
            $table->decimal('X_final_li', 11, 8)->nullable();
            $table->decimal('Y_start_li', 11, 8)->nullable();
            $table->decimal('Y_final_li', 11, 8)->nullable();
            $table->string('avccluster_source',255);
            $table->integer('cms_user_id')->index();
            $table->integer('extid')->nullable();
            $table->boolean('enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avcclusters');
    }
}
