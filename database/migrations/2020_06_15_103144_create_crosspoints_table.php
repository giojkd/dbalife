<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrosspointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crosspoints', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('cross_type',255);
            $table->string('cross_use',255);
            $table->string('structure_code',25)->nullable();
            $table->string('road_code',25)->nullable();
            $table->longText('cross_observations')->nullable();
            $table->longText('notes')->nullable();
            $table->string('cross_source',255);
            $table->decimal('lat', 11, 8);
            $table->decimal('lng', 11, 8);
            $table->integer('cms_user_id')->index();
            $table->integer('extid')->nullable();
            $table->boolean('enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crosspoints');
    }
}
