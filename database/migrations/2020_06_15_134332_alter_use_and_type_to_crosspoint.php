<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUseAndTypeToCrosspoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crosspoints', function (Blueprint $table) {
            $table->string('cross_use')->nullable()->change();
            $table->string('cross_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crosspoints', function (Blueprint $table) {
            $table->string('cross_use')->nullable(true)->change();
            $table->string('cross_type')->nullable(true)->change();
        });
    }
}
