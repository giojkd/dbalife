<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportrows', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('report_id')->index();
            $table->integer('animal_id')->nullable();
            $table->integer('quantity')->nullable()->default(1);
            $table->string('animal_age',255)->nullable();
            $table->string('animal_gender',1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportrows');
    }
}
