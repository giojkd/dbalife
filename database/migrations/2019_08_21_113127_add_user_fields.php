<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserFields extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('cms_users', function (Blueprint $table) {
      $table->string('surname')->nullable();
      $table->string('phone')->nullable();
      $table->string('gender')->nullable();
      $table->date('birth_date')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('cms_users', function (Blueprint $table) {
      //
    });
  }
}
