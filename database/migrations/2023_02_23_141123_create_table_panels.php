<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePanels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panels', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('panel_type',25)->nullable();
            $table->string('road_code',25)->nullable();
            $table->string('data_source',255);
            $table->decimal('lat', 11, 8);
            $table->decimal('lng', 11, 8);
            $table->integer('cms_user_id')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panels');
    }
}
