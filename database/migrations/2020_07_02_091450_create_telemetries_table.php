<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelemetriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemetries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fid')->nullable();
            $table->string('shape',255);
            $table->integer('id_clus')->nullable();
            $table->integer('id_line')->nullable();
            $table->integer('npts_clus')->nullable();
            $table->integer('npts_line')->nullable();
            $table->decimal('strength',11,8)->nullable();
            $table->decimal('str_ci_1',11,8)->nullable();
            $table->decimal('str_ci_2',11,8)->nullable();
            $table->decimal('clus_from',11,8)->nullable();
            $table->decimal('clus_to',11,8)->nullable();
            $table->decimal('len_clus',11,8)->nullable();
            $table->decimal('len_line',11,8)->nullable();
            $table->decimal('dens_point',11,8)->nullable();
            $table->decimal('str_dens2',11,8)->nullable();
            $table->decimal('sd2_ci_1',11,8)->nullable();
            $table->decimal('sd2_ci_2',11,8)->nullable();
            $table->decimal('gstr',11,8)->nullable();
            $table->decimal('gstr_ci_1',11,8)->nullable();
            $table->decimal('gstr_ci_2',11,8)->nullable();
            $table->integer('year')->nullable();
            $table->string('telemetry_source',255);
            $table->integer('cms_user_id')->index();
            $table->integer('extid')->nullable();
            $table->boolean('enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telemetries');
    }
}
