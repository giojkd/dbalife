<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnToTelemetries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telemetries', function (Blueprint $table) {   
            $table->decimal('X_start_li', 11, 8)->nullable()->change();
            $table->decimal('X_final_li', 11, 8)->nullable()->change();
            $table->decimal('Y_start_li', 11, 8)->nullable()->change();
            $table->decimal('Y_final_li', 11, 8)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
