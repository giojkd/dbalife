<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsToTelemetries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telemetries', function (Blueprint $table) {   
            $table->float('strength')->nullable()->change();
            $table->float('str_ci_1')->nullable()->change();
            $table->float('str_ci_2')->nullable()->change();
            $table->float('clus_from')->nullable()->change();
            $table->float('clus_to')->nullable()->change();
            $table->float('len_clus')->nullable()->change();
            $table->float('len_line')->nullable()->change();
            $table->float('dens_point')->nullable()->change();
            $table->float('str_dens2')->nullable()->change();
            $table->float('sd2_ci_1')->nullable()->change();
            $table->float('sd2_ci_2')->nullable()->change();
            $table->float('gstr')->nullable()->change();
            $table->float('gstr_ci_1')->nullable()->change();
            $table->float('gstr_ci_2')->nullable()->change();
            $table->float('X_start_li')->nullable();
            $table->float('X_final_li')->nullable();
            $table->float('Y_start_li')->nullable();
            $table->float('Y_final_li')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
