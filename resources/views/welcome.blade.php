<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Life-safecrossing</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            img {
                border: 0;
                vertical-align: middle;
            }
            .header {
                height: 120px;
                padding :10px
            }

            .header img {
                
                padding :10px
            }



            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                height: 70%;
                background-image: url("/uploads/public/LINCE.jpg");
                background-position: top; /* Center the image */
                background-repeat: no-repeat; /* Do not repeat the image */
                background-size: cover;
                position: relative;
            }

            .footer {
                text-align: center;
            }


            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links-min > a {
                color: #636b6f;
                padding: 0 10px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #footer_loghi img {
                max-width: 60px;
            }

            .d_ib {
                display: inline-block
            }
            .board{
                padding:40px;
                
                position: absolute;
                top: 40%;
                left:50%;
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                -ms-transform: translateX(-50%);
                transform: translateX(-50%);
                width: 600px;
                font-size: 18px;
                font-weight: 200;
                letter-spacing: .2rem;
                background-color: forestgreen ;
                opacity: 0.8;
                border-radius: 5px;
                color:#fff;
            }

        </style>
    </head>
    <body>
        <div class="header">
            <div class="flex-center position-ref full-height">
                <img src="/uploads/public/logo_header.png" style="height:100px">
                <img src="/uploads/public/life.png" style="height:70px">
                <img src="/uploads/public/natura2000.png" style="height:70px">
                <div class="top-right links">
                    @if (CRUDBooster::myID())
                        <a href="{{ url('/admin') }}">Home</a>
                    @else
                        <a href="{{ url('/admin/login') }}">Login</a>
                    @endif
                </div>
            </div> 
        </div>      
        <div class="content">
            <div class="row m_10_v">
                <div class="col-md-10 center">
                        <div class="board">The LIFE SAFE-CROSSING Project aims at implementing actions to reduce the impact of roads on some threatened wildlife species.
                        <br><br>This project is funded with the contribution of the LIFE programme of the European Union</div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="white_bg green center green_bord p_15">
                    <p></p>
                    <p></p>
                    <div class="row m_10_v" id="footer_loghi">
                        <div class="col-xs-12 center links-min">
                            <a href="http://www.agristudiosrl.it" target="_blank" title="View all Agri Studio"><img src="https://life.safe-crossing.eu/static/images/footer_logo_02.png" class="d_ib" alt="agristudiosrl"></a>
                            <a href="http://legacy.callisto.gr/en/callisto.php" target="_blank" title="View all Callisto"><img src="https://life.safe-crossing.eu/static/images/footer_logo_03.png" class="d_ib" alt="callisto"></a>
                            <a href="https://www.cosmote.gr/cs/otegroup/en/cosmote_ae.html" target="_blank" title="View all Cosmote"><img src="https://life.safe-crossing.eu/static/images/footer_logo_04.png" class="d_ib" alt="cosmote"></a>
                            <a href="http://www.egnatia.gr/page/default.asp?la=2" target="_blank" title="View all Egnatia"><img style="max-width: 120px;" src="https://life.safe-crossing.eu/static/images/footer_logo_05.png" class="d_ib" alt="egnatia"></a>
                            <a href="http://www.fundatiacarpati.ro" target="_blank" title="View all Fundati Carpati"><img src="https://life.safe-crossing.eu/static/images/footer_logo_06.png" class="d_ib" alt="fundatiacarpati"></a>
                            <a href="http://en.pdm.gov.gr" target="_blank" title="View all PDM"><img src="https://life.safe-crossing.eu/static/images/footer_logo_07.png" class="d_ib" alt="pdm"></a>
                            <a href="http://icas.ro/" target="_blank" title="View all Icas"><img src="https://life.safe-crossing.eu/static/images/footer_logo_08.png" class="d_ib" alt="icas"></a>
                            <a href="https://minuartia.com/es" target="_blank" title="View all Minuartia"><img style="max-width: 100px;" src="https://life.safe-crossing.eu/static/images/footer_logo_09.png" class="d_ib" alt="minuartia"></a>
                            <a href="http://www.parcoabruzzo.it" target="_blank" title="View all Parco Abruzzo"><img src="https://life.safe-crossing.eu/static/images/footer_logo_10.png" class="d_ib" alt="parco abruzzo"></a>
                            <a href="https://www.parcomajella.it" target="_blank" title="View all Parco Majella"><img src="https://life.safe-crossing.eu/static/images/footer_logo_11.png" class="d_ib" alt="parco majella"></a>
                            <a href="http://cms.provincia.terni.it/on-line/Home.html" target="_blank" title="View all Provincia di Terni"><img src="https://life.safe-crossing.eu/static/images/footer_logo_12.png" class="d_ib" alt="Provincia di Terni"></a>
                            <a href="http://www.juntadeandalucia.es/medioambiente/index_app_no_disponible.html" target="_blank" title="View all Junta de Andalucia"><img src="https://life.safe-crossing.eu/static/images/footer_logo_13.png" class="d_ib" alt="Junta de Andalucia"></a>
                            
                        </div>
                    </div>
            </div>
        </footer>
    </body>
</html>
