<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->
<div class='panel panel-default'>
  <div class='panel-heading'>Edit Form</div>
  <div class='panel-body'>
    <table class="table table-hover table-striped">
      <thead>
        <tr>
          <th></th>
          @foreach($languages as $language)
          <th>
            {{$language['label']}}
          </th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        @foreach($animals as $animalClass)
        <tr>
          <th >{{$animalClass->name}}</th>
          @foreach($languages as $language)
          <td>
            <input value="{{$text['App\Animalclass'][$animalClass->id][$language['id']]['name']}}" data-model_type="App\Animalclass" data-model_id="{{$animalClass->id}}" data-language_id="{{$language['id']}}" type="text" name="" class="form-control texts-field">
          </td>
          @endforeach
        </tr>
        @foreach($animalClass->animals as $animal)
        <tr>
          <td>{{$animal->scientific_name}}</td>
          @foreach($languages as $language)
          <td>
            <input value="{{$text['App\Animal'][$animal->id][$language['id']]['name']}}" data-model_type="App\Animal" data-model_id="{{$animal->id}}" data-language_id="{{$language['id']}}"  type="text" name="" class="form-control texts-field">
          </td>
          @endforeach
        </tr>
        @endforeach
        @endforeach
      </tbody>
    </table>

  </form>
</div>
</div>

<script type="text/javascript">
$('.texts-field').blur(function(){
  var data = $(this).data();
  data.value = $(this).val();
  $.get('/api/text-update',data,function(r){},'json')
})
</script>

@endsection
