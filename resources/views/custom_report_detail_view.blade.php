<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->


<div class='panel panel-default'>
  <div class='panel-body'>
    <h3>Summary</h3>
    <table class="table table-hover table-striped">
      <tbody>
        <tr>
          <td><label for="">Date</label></td><td>{{$created_at}}</td>
        </tr>
        <tr>
          <td><label for="">Animal</label></td><td>{{$animal}}</td>
        </tr>
        <tr>
          <td><label for="">Note</label></td><td>{{$notes}}</td>
        </tr>
        <tr>
          <td><label for="">Type</label></td><td>{{$type}}</td>
        </tr>
        <tr>
          <td><label for="">Lat</label></td><td>{{$lat}}</td>
        </tr>
        <tr>
          <td><label for="">Lng</label></td><td>{{$lng}}</td>
        </tr>
      </tbody>
    </table>
  </form>
  <h3>Photos</h3>
  @foreach($files as $file)
  <img src="{{$file}}" alt="" style= "height: 320px;"/>
  @endforeach
  <h3>Map</h3>
  <div id="map" style="height: 480px; width: 640px;"></div>

</div>
</div>



<script>
var map;
var goldStar = {
          path: ' M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
          fillColor: '#4545445',
          fillOpacity: 0.8,
          scale: 0.1,
          strokeColor: 'white',
          strokeWeight: 1
        };
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: <?=$lat?>, lng: <?=$lng?>},
    zoom: 14,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    mapTypeId: 'satellite'
  });
  goldStar.fillColor = '#'+intToRGB(hashCode('<?=$animal?>'));
  var marker = new google.maps.Marker({
    position: {lat: <?=$lat?>, lng: <?=$lng?>},
    icon: goldStar,
    title:"<?=$animal?>"
  });
  marker.setMap(map);
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1m5KmsI_YA9Rc7NRZ_g4NhbiHAED89BE&callback=initMap" async defer></script>


@endsection
