<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<style>
  .map-filter-block {
    display:block;
    overflow-y: auto;
    border: solid 1px #ccc;
    border-radius: 5px;
    padding: 5px;

  }
  #myInput {
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 5px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}

</style>
<!-- Your html goes here -->
<div class='panel panel-default'>
    <div class='panel-body'>
      <div class="row">
          <div class="col-md-2">
            <h3>Filters</h3>
            <h4>Species</h4>
            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for species...">
            <div class="map-filter-block" style="height:250px">
              <label><input id="animal-cb-all"  type="checkbox" checked value="1"> check/uncheck all</span></label>
              <hr style="margin:0;padding:5px">
              <ul id="myUL" class="sidebar-menu">
                @foreach($animals as $animal)
                  <li><label><input class="animal-cb"  type="checkbox" checked value="<?=$animal['scientific_name']?>"> <?=$animal['scientific_name']?></span></label></li>
                @endforeach
              </ul>
            </div>
            <h4>Data source</h4>
            <div class="map-filter-block">
              <ul class="sidebar-menu">
                @foreach($sources as $source)
                  <li><label><input class="source-cb" type="checkbox" checked value="<?=$source['report_source']?>"> <?=$source['report_source']?></span></label></li>
                @endforeach
              </ul>
            </div>
            <h4>Data type</h4>
            <div class="map-filter-block">
              <ul class="sidebar-menu">
                @foreach($types as $type)
                  <li><label><input class="type-cb" type="checkbox" checked value="<?=$type['name']?>"> <?=$type['name']?></span></label></li>
                @endforeach
              </ul>
            </div>
            <h4>Output data</h4>
            <div class="map-filter-block">
              <ul class="sidebar-menu">
                <li><label><input class="kind-cb" type="checkbox" checked value="animal"> Reports</span></label></li>
                <li><label><input class="kind-cb" type="checkbox" checked value="avccluster"> AVC clusters</span></label></li>
                <li><label><input class="kind-cb" type="checkbox" checked value="cross"> Crossing structures</span></label></li>
                <li><label><input class="kind-cb" type="checkbox" checked value="telemetry"> Crossing points clusters</span></label></li>
                <li><label><input class="kind-cb" type="checkbox" checked value="panel"> Road panels</span></label></li>
                <li><label><input class="kind-cb" type="checkbox" checked value="avcps"> AVC PS</span></label></li>
                <hr style="margin:0;padding:5px">
                <li><label>Crossing points</label>
                  <ul class="sidebar-menu" style="padding-left: 10px">
                    @foreach($cpoints as $key => $cpoint)
                      <li><label><input class="kind-cb" type="checkbox" value="cpoint_<?=$key?>"> Crossing points {{ $partners[$key] }}</span></label></li>
                    @endforeach
                  </ul>
                </li>
            </div>
          </div>
          <div class="col-md-10">
            <h3>Map</h3>
            <div id="map" style="height: 800px; width: 100%;"></div>
          </div>
      </div>
    </div>
</div>
<script>
var infowindow;
var map;
var drawingManager;
var rectangle;
var markers = new Array();
var tellines = new Array();
var reset = {lat: 41.902782, lng: 12.496366}
var crossair = {
          path: 'm 0 0 h -40 v -20 h 40 v -40 h 20 v 40 h 40 v 20 h -40 v 40 h -20 v -40 z',
          fillColor: 'red',
          fillOpacity: 0.8,
          scale: 0.07,
          strokeColor: 'black',
          strokeWeight: 0.5
        };
var crossairgreen = {
      path: 'm 0 0 h -40 v -20 h 40 v -40 h 20 v 40 h 40 v 20 h -40 v 40 h -20 v -40 z',
      fillColor: 'green',
      fillOpacity: 0.8,
      scale: 0.07,
      strokeColor: 'black',
      strokeWeight: 0.5
};
var panelmarker = {
      path: 'm 0 0 h -40 v -20 h 40 v -40 h 20 v 40 h 40 v 20 h -40 v 40 h -20 v -40 z',
      fillColor: 'gold',
      fillOpacity: 0.8,
      scale: 0.10,
      strokeColor: 'black',
      strokeWeight: 0.5
};
var avcpsmarker = {
      path: 'm 0 0 h -40 v -20 h 40 v -40 h 20 v 40 h 40 v 20 h -40 v 40 h -20 v -40 z',
      fillColor: 'silver',
      fillOpacity: 0.8,
      scale: 0.10,
      strokeColor: 'black',
      strokeWeight: 0.5
};
var goldStar = {
          path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
          fillColor: '#4545445',
          fillOpacity: 0.8,
          scale: 0.07,
          strokeColor: 'black',
          strokeWeight: 1
        };
var reports = new Array();
var crosses = new Array();
var panels = new Array();
var telemetries = new Array();
var cpoints = new Array();
var avcclusters = new Array();
var animal = new Array();
var avcps = new Array();
var displayable = new Object();
var item = {};
/* --- loading values from db */


@foreach($reports as $report)
  item = {'id':<?=$report['id']?>,
  'created_at':'<?=$report['created_at'] ?>',
  'lat':'<?=$report['lat']?>',
  'lng':'<?=$report['lng']?>',
  'type':'<?=$report['type']?>',
  'animal':'<?=$report['animal']?>',
  'kind':'<?=$report['kind']?>',
  'source':"<?=$report['source']?>"};
  reports.push(item);
@endforeach

@foreach($crosses as $cross)
  item = {'id':<?=$cross['id']?>,
  'created_at':'<?=$cross['created_at'] ?>',
  'lat':'<?=$cross['lat']?>',
  'lng':'<?=$cross['lng']?>',
  'type':'<?=$cross['type']?>',
  'kind':'<?=$cross['kind']?>',
  'source':"<?=$cross['source']?>"};
  crosses.push(item);
@endforeach

@foreach($panels as $panel)
  item = {'id':<?=$panel['id']?>,
  'created_at':'<?=$panel['created_at'] ?>',
  'lat':'<?=$panel['lat']?>',
  'lng':'<?=$panel['lng']?>',
  'type':'<?=$panel['type']?>',
  'kind':'<?=$panel['kind']?>',
  'source':"<?=$panel['source']?>"};
  panels.push(item);
@endforeach

@foreach($avcpss as $avcps)
  item = {'id':<?=$avcps['id']?>,
  'created_at':'<?=$avcps['created_at'] ?>',
  'lat':'<?=$avcps['lat']?>',
  'lng':'<?=$avcps['lng']?>',
  'type':'<?=$avcps['type']?>',
  'kind':'<?=$avcps['kind']?>',
  'source':"<?=$avcps['source']?>"};
  avcps.push(item);
@endforeach

@foreach($telemetries as $telemetry)
  item = {'id':<?=$telemetry['id']?>,
  'created_at':'<?=$telemetry['created_at'] ?>',
  'lat_start':'<?=$telemetry['lat_start']?>',
  'lat_end':'<?=$telemetry['lat_end']?>',
  'lng_start':'<?=$telemetry['lng_start']?>',
  'lng_end':'<?=$telemetry['lng_end']?>',
  'animal':'<?=$telemetry['animal']?>',
  'kind':'<?=$telemetry['kind']?>',
  'year':'<?=$telemetry['year']?>',
  'source':"<?=$telemetry['source']?>"};
  telemetries.push(item);
@endforeach

@foreach($avcclusters as $avcclusrter)
  item = {'id':<?=$avcclusrter['id']?>,
  'created_at':'<?=$avcclusrter['created_at'] ?>',
  'lat_start':'<?=$avcclusrter['lat_start']?>',
  'lat_end':'<?=$avcclusrter['lat_end']?>',
  'lng_start':'<?=$avcclusrter['lng_start']?>',
  'lng_end':'<?=$avcclusrter['lng_end']?>',
  'animal':'<?=$avcclusrter['animal']?>',
  'kind':'<?=$avcclusrter['kind']?>',
  'year':'<?=$avcclusrter['year']?>',
  'source':"<?=$avcclusrter['source']?>"};
  avcclusters.push(item);
@endforeach

@foreach($cpoints as $key => $cpointpartners) 
  @foreach($cpointpartners as $cpoint)
    item = {'id':<?=$cpoint['id']?>,
    'lat':'<?=$cpoint['lat']?>',
    'lng':'<?=$cpoint['lng']?>',
    'animal':'<?=$cpoint['animal']?>',
    'kind':'<?=$cpoint['kind']?>'};
    cpoints.push(item);
  @endforeach
@endforeach

displayable['animal'] = [];
displayable['source'] = [];
displayable['type'] = [];
displayable['kind'] = [];
@foreach($animals as $animal)
  animal.push('<?=$animal['scientific_name']?>');
@endforeach
displayable['animal'] = animal;
@foreach($sources as $source)
  displayable['source'].push("<?=$source['report_source']?>");
@endforeach
@foreach($types as $type)
  displayable['type'].push("<?=$type['name']?>");
@endforeach
displayable['kind'].push('animal');
displayable['kind'].push('cross');
displayable['kind'].push('telemetry');
displayable['kind'].push('avccluster');
displayable['kind'].push('panel');
displayable['kind'].push('avcps');
/* --- end loading --- */
console.log(displayable);
$(function() {
    $('#animal-cb-all').click(function(){
        if($(this).is(":checked")){
          $('.animal-cb').prop("checked", true);
          displayable['animal'] = animal;
        }
        else if($(this).is(":not(:checked)")){
          $('.animal-cb').prop("checked", false);
          displayable['animal'] = [];
        }
        clearMarkers();
        getSpot();
        getCross();
        getTelemeries();
        getCpoints();
        getPanels();
        getAvcclusters();
        getAvcPss();
    });
    $('.animal-cb').click(function(){
        if($(this).is(":checked")){
          displayable['animal'].push($(this).val());
          console.log(displayable);
        }
        else if($(this).is(":not(:checked)")){
          displayable['animal'].splice(displayable['animal'].indexOf($(this).val()), 1);
          console.log(displayable);
        }
        clearMarkers();
        getSpot();
        getCross();
        getTelemeries();
        getCpoints();
        getPanels();
        getAvcclusters();
        getAvcPss();
    });
    $('.source-cb').click(function(){
        if($(this).is(":checked")){
          displayable['source'].push($(this).val());
          console.log(displayable);
        }
        else if($(this).is(":not(:checked)")){
          displayable['source'].splice(displayable['source'].indexOf($(this).val()), 1);
          console.log(displayable);
        }
        clearMarkers();
        getSpot();
        getCross();
        getTelemeries();
        getCpoints();
        getPanels();
        getAvcclusters();
        getAvcPss();
    });
    $('.type-cb').click(function(){
        if($(this).is(":checked")){
          displayable['type'].push($(this).val());
          console.log(displayable);
        }
        else if($(this).is(":not(:checked)")){
          displayable['type'].splice(displayable['type'].indexOf($(this).val()), 1);
          console.log(displayable);
        }
        clearMarkers();
        getSpot();
        getCross();
        getTelemeries();
        getCpoints();
        getPanels();
        getAvcclusters();
        getAvcPss();
    });
    $('.kind-cb').click(function(){
        if($(this).is(":checked")){
          displayable['kind'].push($(this).val());
          console.log(displayable);
        }
        else if($(this).is(":not(:checked)")){
          displayable['kind'].splice(displayable['kind'].indexOf($(this).val()), 1);
          console.log(displayable);
        }
        clearMarkers();
        getSpot();
        getCross();
        getTelemeries();
        getCpoints();
        getPanels();
        getAvcclusters();
        getAvcPss();
    });
});



function myFunction() {
  // Declare variables
  var input, filter, ul, li, label, i, txtValue;
  input = document.getElementById('myInput');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('li');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    label = li[i].getElementsByTagName("label")[0];
    txtValue = label.textContent || label.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

/**
 * The CenterControl adds a control to the map that recenters the map on
 * Chicago.
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
 function CenterControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    //controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '8px';
    controlUI.style.marginBottom = '10px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to reset the map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.textContent = 'Reset zoom and pan';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function() {
      pan(reset,5);
    });

}

function initMap() {
  infowindow = new google.maps.InfoWindow({});
  var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/info-i_maps.png';
  map = new google.maps.Map(document.getElementById('map'), {
    center: reset,
    zoom: 5,
  });
  var centerControlDiv = document.createElement('div');
  var centerControl = new CenterControl(centerControlDiv, map);

  centerControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);
  drawingManager = new google.maps.drawing.DrawingManager({
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: ["rectangle"]
    },
    markerOptions: {
      icon:
        "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"
    },
   
  });
  drawingManager.setMap(map);
  google.maps.event.addListener(drawingManager, 'overlaycomplete', function (overlay) {
    rectangle = overlay;
    drawingManager.setOptions({
      drawingMode: null,
    });
    rectanglezoom(overlay);
  });
  getSpot();
  getCross();
  getTelemeries();
  getCpoints();
  getPanels();
  getAvcclusters();
  getAvcPss();
}




function pan(apoint,azoom) {
  var panPoint = new google.maps.LatLng(apoint.lat, apoint.lng);
  map.setZoom(azoom); // or whatever lvl of zoom desired
  map.panTo(panPoint);
}

function rectanglezoom(arectangle) {
 
  var bounds = arectangle.overlay.getBounds();
  var start = bounds.getNorthEast();
  var end = bounds.getSouthWest();
  var center = bounds.getCenter();
  arectangle.overlay.setMap(null);
  console.log(center.toUrlValue(6));
  var latlng = center.toUrlValue(6);
  var coord = latlng.split(",")
  var newcent = {lat: coord[0], lng: coord[1]};
  map.fitBounds(bounds);
  //pan(newcent,10);
  /*var panPoint = new google.maps.LatLng(apoint.lat, apoint.lng);
  map.setZoom(5); // or whatever lvl of zoom desired
  map.panTo(panPoint);*/
}

function getSpot() {
  reports.forEach(function(entry) {
    if (!checkfilter(entry,'animal')) return;
    if (!checkfilter(entry,'source')) return;
    if (!checkfilter(entry,'type')) return;
    if (!checkfilter(entry,'kind')) return;
      goldStar.fillColor = '#'+intToRGB(hashCode(entry.animal));
      var myLatlng = new google.maps.LatLng(entry.lat,entry.lng);
      var marker = new google.maps.Marker({
              position: myLatlng,
              icon: goldStar,
              title:entry.animal,
              id:entry.id,
              map:map,
      });
    markers.push(marker);
    marker.addListener('mouseover', function() {
      infowindow.setContent("<div><b>"+marker.title+"</b><br>"
        +"Lat:<b>"+marker.getPosition().lat()+ "</b> | Lng:<b>"+marker.getPosition().lng()+"</b><br>"
        +"source:<b>"+entry.source+"</b><br>"
        +"date:<b>"+entry.created_at+"</b><br>"
        +"type:<b>"+entry.type+"</b>"
        +"</div>");
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'click', function(e) {
        window.location.href = "/admin/reports/detail/"+this.id;
    });
  });                                            
}

function getCross() {
  crosses.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
      var myLatlng = new google.maps.LatLng(entry.lat,entry.lng);
      var marker = new google.maps.Marker({
              position: myLatlng,
              icon: crossair,
              title:entry.type,
              id:entry.id,
              map:map,
      });
    markers.push(marker);
    marker.addListener('mouseover', function() {
      infowindow.setContent("<div><b>"+marker.title+"</b><br>"
        +"Lat:<b>"+marker.getPosition().lat()+ "</b> | Lng:<b>"+marker.getPosition().lng()+"</b><br>"
        +"source:<b>"+entry.source+"</b><br>"
        +"date:<b>"+entry.created_at+"</b><br>"
        +"</div>");
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'click', function(e) {
        window.location.href = "/admin/crosspoints/detail/"+this.id;
    });
  });                                            
}

function getPanels() {
  panels.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
      var myLatlng = new google.maps.LatLng(entry.lat,entry.lng);
      var marker = new google.maps.Marker({
              position: myLatlng,
              icon: panelmarker,
              title:entry.type,
              id:entry.id,
              map:map,
      });
    markers.push(marker);
    marker.addListener('mouseover', function() {
      infowindow.setContent("<div><b>"+marker.title+"</b><br>"
        +"Lat:<b>"+marker.getPosition().lat()+ "</b> | Lng:<b>"+marker.getPosition().lng()+"</b><br>"
        +"source:<b>"+entry.source+"</b><br>"
        +"date:<b>"+entry.created_at+"</b><br>"
        +"</div>");
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'click', function(e) {
        window.location.href = "/admin/panels/detail/"+this.id;
    });
  });                                            
}

function getAvcPss() {
  avcps.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
      var myLatlng = new google.maps.LatLng(entry.lat,entry.lng);
      var marker = new google.maps.Marker({
              position: myLatlng,
              icon: avcpsmarker,
              title:entry.type,
              id:entry.id,
              map:map,
      });
    markers.push(marker);
    marker.addListener('mouseover', function() {
      infowindow.setContent("<div><b>"+marker.title+"</b><br>"
        +"Lat:<b>"+marker.getPosition().lat()+ "</b> | Lng:<b>"+marker.getPosition().lng()+"</b><br>"
        +"source:<b>"+entry.source+"</b><br>"
        +"date:<b>"+entry.created_at+"</b><br>"
        +"</div>");
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'click', function(e) {
        window.location.href = "/admin/avcps/detail/"+this.id;
    });
  });                                            
}

function getTelemeries() {
  telemetries.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
    var myLatlngstart = new google.maps.LatLng(entry.lat_start,entry.lng_start);
    var myLatlngend = new google.maps.LatLng(entry.lat_end,entry.lng_end);
      var telCoordinates = [
        myLatlngstart,
        myLatlngend 
      ];
      var telPath = new google.maps.Polyline({
        path: telCoordinates,
        geodesic: true,
        strokeColor: "green",
        strokeOpacity: 1.0,
        strokeWeight: 4,
        id:entry.id,
        map:map,
      });
      tellines.push(telPath);
      telPath.addListener('mouseover', function() {
        infowindow.setContent("<div>"
          +"source:<b>"+entry.source+"</b><br>"
          +"year:<b>"+entry.year+"</b><br>"
          +"specie:<b>"+entry.animal+"</b><br>"
          +"</div>");
        infowindow.setPosition(myLatlngstart);
        infowindow.open(map, telPath);
    });
    telPath.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(telPath, 'click', function(e) {
        window.location.href = "/admin/telemetries/detail/"+this.id;
    });
  });                                            
}

function getAvcclusters() {
  avcclusters.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
    var myLatlngstart = new google.maps.LatLng(entry.lat_start,entry.lng_start);
    var myLatlngend = new google.maps.LatLng(entry.lat_end,entry.lng_end);
      var telCoordinates = [
        myLatlngstart,
        myLatlngend 
      ];
      var telPath = new google.maps.Polyline({
        path: telCoordinates,
        geodesic: true,
        strokeColor: "#ff0000",
        strokeOpacity: 1.0,
        strokeWeight: 4,
        id:entry.id,
        map:map,
      });
      tellines.push(telPath);
      telPath.addListener('mouseover', function() {
        infowindow.setContent("<div>"
          +"source:<b>"+entry.source+"</b><br>"
          +"year:<b>"+entry.year+"</b><br>"
          +"</div>");
        infowindow.setPosition(myLatlngstart);
        infowindow.open(map, telPath);
    });
    telPath.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(telPath, 'click', function(e) {
        window.location.href = "/admin/telemetries/detail/"+this.id;
    });
  });                                            
}

function getCpoints() {
  cpoints.forEach(function(entry) {
    if (!checkfilter(entry,'kind')) return;
      var myLatlng = new google.maps.LatLng(entry.lat,entry.lng);
      var marker = new google.maps.Marker({
              position: myLatlng,
              icon: crossairgreen,
              id:entry.id,
              map:map,
      });
    markers.push(marker);
    marker.addListener('mouseover', function() {
      infowindow.setContent("<div>"
        +"Lat:<b>"+marker.getPosition().lat()+ "</b> | Lng:<b>"+marker.getPosition().lng()+"</b><br>"
        +"species:<b>"+entry.animal+"</b><br>"
        +"</div>");
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'click', function(e) {
        window.location.href = "/admin/cpoints/detail/"+this.id;
    });
  });                                            
}


function checkfilter(entry, dimension) {
  var rt = displayable[dimension].includes(entry[dimension]);
  return rt;
}

// Sets the map on all markers in the array.
function setMapOnAll(amap) {
  for (var i = 0; i < markers.length; i++) {
     markers[i].setMap(amap);
  }
  for (var i = 0; i < tellines.length; i++) {
     tellines[i].setMap(amap);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1m5KmsI_YA9Rc7NRZ_g4NhbiHAED89BE&callback=initMap&libraries=drawing" async defer></script>
@endsection
