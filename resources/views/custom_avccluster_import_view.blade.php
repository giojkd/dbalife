<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->

@include('flash-message')



<div class='panel panel-default'>
    <div class='panel-body'>
        @if (!$message = Session::get('success'))
            <h3>Step 1: Upload a file</h3>
            <hr>
            <p>NB: the import file must be in the exact format of the sample file which you can download here.</p>
            <a href="/uploads/public/import_avccluster.csv" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> Download exemple</a>
            <hr>
            <form action="/admin/avcclusters-import" method="POST" enctype="multipart/form-data"> {{ csrf_field() }}
                <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="file" name="importing-file" required>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="submit" name="Upload">Upload</button> 
                                </td>
                            </tr>
                        </tbody>
                </table>
            </form>
        @else
        <h3>Step 2: Import data</h3>
        <hr>
        <a href="/admin/avcclusters-seeding" class="btn btn-default">Import data</a>
        @endif
    </div>
</div>

<script>
</script>


@endsection
